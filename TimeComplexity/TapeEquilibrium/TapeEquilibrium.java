package TapeEquilibrium;

public class TapeEquilibrium {
	public int solution(int[] A) {
		long rsum = 0;
		for (int i=1; i < A.length; i++)
			rsum += A[i];
		
		int min = (int) Math.abs(rsum - A[0]);
		long lsum = A[0];
		for (int i=1; i < (A.length - 1); i++) {
			lsum += A[i];
			rsum -= A[i];
			
			int diff = (int) Math.abs(lsum - rsum);
			if (diff < min)
				min = diff;
		}
			
		return min;
	}
}