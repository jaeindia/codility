package FrogJmp;

public class Solution {
	public int solution(int X, int Y, int D) {
		return ((int) Math.ceil((Math.abs(Y - X) * 1.0 / D)));
	}
}
