package PermMissingElem;

public class Solution {
	public int solution(int[] A) {
		long N = A.length + 1;
		long expectedSum = (N * (N + 1)) / 2;
		for (int x : A) {
			expectedSum -= x;
		}
		return (int) expectedSum;
	}
}
