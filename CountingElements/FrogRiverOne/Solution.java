package FrogRiverOne;

public class Solution {
	public int solution(int X, int[] A) {
		boolean[] boolArr = new boolean[X];
		int time = 0;
		int pos = 0;
		for (int element : A) {
			if ((element <= X) && !boolArr[element - 1]) {
				boolArr[element - 1] = true;
				pos++;
				if (pos == X)
					return time;
			}
			time++;
		}
		return -1;
	}
}
