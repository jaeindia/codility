package MaxCounters;

import java.util.Arrays;

class Solution {
	public int[] solution(int N, int[] A) {
		int counter[] = new int[N];
		Arrays.fill(counter, 0);
		int max = 0;
		int updateCounter = 0;

		for (int x : A) {
			if (x >= 1 && x <= N) {
				counter[x - 1] = Math.max(updateCounter, counter[x - 1]) + 1;
				max = Math.max(max, counter[x - 1]);
			}
			if (x == N + 1)
				updateCounter = max;
		}
		
		for (int i=0; i<counter.length; i++) {
			if (counter[i] < updateCounter)
				counter[i] = updateCounter;
		}

		return counter;
	}
}
