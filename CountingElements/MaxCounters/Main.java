package MaxCounters;

public class Main {

	public static void main(String[] args) {
		Solution obj = new Solution();
		
		int N = 5;
		int A[] = new int[7];
		A[0] = 3;
		A[1] = 4;
		A[2] = 4;
		A[3] = 6;
		A[4] = 1;
		A[5] = 4;
		A[6] = 4;
		
		for (int x : obj.solution(N, A)) {
			System.out.format("%d ", x);
		}
	}
}
