package MissingInteger;
import java.util.HashMap;

public class Solution {
	 public int solution(int[] A) {
		 int max = 1;
		 HashMap<Integer, Boolean> positiveMap = new HashMap<Integer, Boolean>(); 
		 
		 for (int x : A) {
			 if (x > 0) {
				 positiveMap.put(x, true);
				 if (max < x)
					 max = x;
			 }
		 }
		 
		 for (int i = 1; i <= max; i++) {
			 if (!positiveMap.containsKey(i))
				 return i;
		 }
		 
		 return max+1;
	 }
}
