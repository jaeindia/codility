package PermCheck;

public class Solution {
	public int solution(int[] A) {
		int N = A.length;
		boolean[] boolArr = new boolean[N];
		int count = 0;
		
		for (int x : A) {
			if (x > N)
				return 0;
			
			if (x > 0) {
				if (!boolArr[x-1]) {
					boolArr[x-1] = true;
					count ++;
					
					if (count == N)
						return 1;
				}
				else
					return 0;
			}
			else {
				return 0;
			}
		}
		
		return 0;
	}
}
