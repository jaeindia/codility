package Dominator;

public class Solution {
	public int solution(int[] A) {
		int N = A.length;
		int element = N > 0 ? A[0] : 0;
		int counter = 0;

		int index = -1;

		for (int x : A) {
			if (element == x)
				counter++;
			else {
				if (counter == 0) {
					element = x;
					counter++;
				} else
					counter--;
			}
		}

		counter = 0;

		for (int i = 0; i < N; i++) {
			if (element == A[i]) {
				counter++;
				index = i;
			}
		}

		if (counter > (N / 2))
			return index;

		return -1;
	}
}
