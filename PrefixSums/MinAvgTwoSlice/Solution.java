package MinAvgTwoSlice;

public class Solution {
	public int solution(int[] A) {
		int N = A.length - 1;
		int index = 0;
		double minAvg = (A[0] + A[1]) / 2.0;
		double tempAvg = Integer.MAX_VALUE;
		
		for (int i = 0; i <= N - 2; i++) {
			tempAvg = (A[i] + A[i + 1]) / 2.0;
			if (tempAvg < minAvg) {
				minAvg = tempAvg;
				index = i;
			}

			tempAvg = (A[i] + A[i + 1] + A[i + 2]) / 3.0;
			if (tempAvg < minAvg) {
				minAvg = tempAvg;
				index = i;
			}
		}
		tempAvg = (A[N - 1] + A[N]) / 2.0;
		if (tempAvg < minAvg)
			index = N - 1;

		return index;
	}
}
