package PassingCars;

public class Main {

	public static void main(String[] args) {
		Solution obj = new Solution();
		int A[] = new int[5];
		A[0] = 0;
		A[1] = 1;
		A[2] = 0;
		A[3] = 1;
		A[4] = 1;

		System.out.println(obj.solution(A));

	}

}
