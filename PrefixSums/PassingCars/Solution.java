package PassingCars;

class Solution {
	public int solution(int[] A) {
		long ctr = 0;
		int j = 0;
		
		for (int i = A.length - 1; i >= 0; i--) {
			if (A[i] == 1)
				j += 1;
			
			if (A[i] == 0)
				ctr += j;
			
		}
		
		if (ctr > 1000000000)
			return -1;
		
		return (int) ctr;
	}
}
