package CountDiv;

class Solution {
	public int solution(int A, int B, int K) {
		
		return (B / K) - (A > 0 ? (A - 1) / K : -1);
	}
}
